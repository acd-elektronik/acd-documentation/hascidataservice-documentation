# ACD HasciDataService documentation  

This page serves to provide documentation and code examples for interacting with the API of ACD HasciDataService.  
The ACD HasciDataService is an Android service application meant to interact with a HasciSE.

## Content of this Project

### Documentation

[ACD HasciDataService documentation](documentation.md)  

### Examples  

[Broadcast Receiver Example](BroadcastReceiverExample.java)  

[Service Communication Example](ServiceCommunicationExample.java)