// Example of a BroadcastReceiver
public class BarcodeReceiver extends BroadcastReceiver {

    public static final String ACTION_BARCODE = 
      "de.acdgruppe.hasciservice.data";  // In HasciDataService configured
                                         // Intent Action
  
    private static final String TAG = "BarcodeReceiver";
  
    @Override
    public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();
      Bundle b = intent.getExtras();
  
      Log.d(TAG, "onReceive");
      if (action.equals(ACTION_BARCODE)) {
        Log.d(TAG, String.format(Locale.US,
                  "Action equals ACTION_BARCODE: %s", ACTION_BARCODE));
  
        try {
          displayScanResult(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
      }
    }
  
    private void displayScanResult(Intent initiatingIntent)
    {
      String decodedData = initiatingIntent.getStringExtra(
             "de.acdgruppe.hasciservice.barcode.data");
  
      Log.i(TAG, "barcodeData: "+decodedData);
  
      String barcodeType = initiatingIntent.getStringExtra(
             "de.acdgruppe.hasciservice.barcode.type");
  
      Log.i(TAG, "barcodeType: "+barcodeType);
  
      MainActivity._txtBarcodeData.setText(decodedData);
      MainActivity._txtBarcodeType.setText(barcodeType);
    }
}

// Example of using the BroadcastReceiver in an Activity
public class MainActivity extends AppCompatActivity {
  
    public static TextView _txtBarcodeData;
    public static TextView _txtBarcodeType;
  
    BarcodeReceiver _barcodeReceiver;
    IntentFilter _barcodeIntentFilter;
  
    @Override
    protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);
  
      _txtBarcodeData = (TextView) findViewById(R.id.txtBarcodeData);
      _txtBarcodeType = (TextView) findViewById(R.id.txtBarcodeType);
  
      _barcodeReceiver = new BarcodeReceiver();
  
       _barcodeIntentFilter = new IntentFilter(
           BarcodeReceiver.ACTION_BARCODE);  // In HasciDataService configured
                                             // Intent Action
  
      _barcodeIntentFilter.addCategory(
          Intent.CATEGORY_DEFAULT);         // In HasciDataService configured
                                            // Intent Categoty
  
      registerReceiver(_barcodeReceiver, _barcodeIntentFilter);
    }
}  