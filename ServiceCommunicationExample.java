 public class ServiceConnection {

    // Example of a Service connection object called moduleConnection
    private ServiceConnection moduleConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
      
            try {
                Log.v(TAG, "send initial WHAT_HELLO");
                moduleMessenger = new Messenger(service);
                sendServiceMessage(null, WHAT_HELLO, 0, 0);
            } catch (Exception e) {
                Log.e(TAG, "false, Error on init M2ModuleService");
            }
        }
      
        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
    };


    // Example of a messenger to communicate with the service and how to handle some message examples
    private Messenger moduleReceiver = new Messenger(new Handler(new Callback() {
        @Override
        public boolean handleMessage(Message msg) {
      
            Log.v(TAG, msg.toString());
            Bundle bundle = msg.getData();
      
            // extract data depending on the content of msg.what
            switch (msg.what) {
                case WHAT_OK:
                    Log.i(TAG, "response from service was WHAT_OK");
                    Bundle bundle = msg.getData();
                    // Handle WHAT Message
                    break;
            

                case WHAT_HASCI_SCANDATA:
                    String scanData = bundle.getString("scanData");
                    Log.v(TAG, "scanData: "+scanData);
            
                    String barcodeType =  bundle.getString("barcodeType");
                    if (null != barcodeType)
                        Log.v(TAG, "barcodeType: "+barcodeType);
                    break;

                case WHAT_HASCI_CONFIG:
                    Log.i(TAG, "received WHAT_HASCI_CONFIG");
                  
                    String configData = bundle.getString("configData");
                    Log.v(TAG, "configData: "+configData);
                    break;
                  
            
                // ...
            }
            return true;
        }
    }));
       

    // Example on how to bind to the service
    public void bindService() {
        try {
            Intent acdIntent = new Intent();
            ComponentName acdName = new ComponentName(
              "de.acdgruppe.hasciservice",
              "de.acdgruppe.hasciservice.HasciService");
            acdIntent.setComponent(acdName);
            if (this.getApplicationContext().bindService(
                acdIntent, moduleConnection, Context.BIND_AUTO_CREATE)) {
              Log.i(TAG, "HasciService connect");
              isConnected = true;
            } else {
              Log.w(TAG, "false, can't connect to HasciService");
            }
        } catch (Exception e) {
              Log.e(TAG, "false, can't connect to HasciService | " + e.getMessage());
        }    
    }


    // Example of a method to send messages to the service
    private boolean sendServiceMessage(Bundle bundle, int what, int arg1, int arg2){
        boolean result = false;
        try {
            Message moduleMessage = Message.obtain(null, what, arg1, arg2);
      
            if (null != bundle) {
                System.out.println("bundle data: " + bundle.getString("configCommand"));
                moduleMessage.setData(bundle);
            }
      
            moduleMessage.replyTo = moduleReceiver;
            moduleMessenger.send(moduleMessage);
            moduleMessage.getCallback();
            result = true;
        } catch (Exception e) {
            Log.e(TAG, "false, Error on HasciService communication");
        }
        return result;
    }
    

    // Example of how to configure the Hasci
    public void configureHasci(String command) {
        Bundle bundle = new Bundle();
        String configCommand = command + "\n";
        bundle.putString("configCommand", configCommand);
        sendServiceMessage(bundle, WHAT_HASCI_CONFIG, 0,0);
    }
}