### ACD HasciDataService Documentation  
### Documentation on how to use the service  
### Version: 1.00

![](images/icon.png)

**Content**

1. App Programming  
 1.1. Receiving the Broadcast Intent  
 1.2. Using of a Messenger  
  1.2.1. Bind on a service  
  1.2.2. The ServiceConnection  
  1.2.3. Initialize Communication  
  1.2.4. Receive messages  
  1.2.5. Answer messages  
  1.2.6. Send messages  
   1.2.6.1. Scan and Connect  
   1.2.6.2. Configurate HasciSE  
 1.3. WHAT Messages  
  1.3.1. Message Structure  
  1.3.2. The message IDs  
  1.3.3. Message WHAT_HELLO  
  1.3.4. Response messages to the app  
2. Definition of the Message Bundle  
 2.1. Receiving Scan Data in SSI Mode  
  2.1.1. Barcode Types as Text  
 2.2. Receiving Scan Data in Raw Mode  
  2.2.1. Barcode IDs as Text  
 2.3. Structure of Commands and Responses  
  2.3.1. Sending Commands  
  2.3.2. Receiving responses  
 2.4. Switch Commands  
  2.4.1. TRIGGER Command  
  2.4.2. SHUTDOWN Command  
  2.4.3. VIBRATE Command  
  2.4.4. BEEP Command  
  2.4.5. AUTOTRIGGER Command  
  2.4.6. SCANDELAY Command  
 2.5. Querying Basic Information  
  2.5.1. Query of Basic Information  
  2.5.2. Basic Information Response  
 2.6. Barcode Activation and Query  
  2.6.1. Barcode Activation  
  2.6.2. Query Barcode Activation Status  
  2.6.3. Barcode Activation Status Response  
  2.6.4. Names for the Barcode Activation and Query  
 2.7. Querying Device Attributes  
  2.7.1. Query of the Devices Attributes  
  2.7.2. Response with the Attributes  
3. Definition of Message Bundles for the Firmware Update  
 3.1. Starting a Firmware Update  
 3.2. Response to Starting the Firmware Update  
 3.3. Possible Errors When Starting the Firmware Update  
 3.4. Messages During the Firmware Update  
 3.5. Possible Errors During the Firmware Update  
4. Notices  
 4.1. Variants of the HasciDataService  
 4.2. Authorizations and Difference between System App and Normal App  
 4.3. Recommendation for Installation  

  
## App Programming

### Receiving the Broadcast Intent

To receive the Broadcast Intent of the scanned barcode sent by the HasciDataService in a receiving app, first make certain the settings _"Intent Action"_ and _"Intent Category"_ of HasciDataService are correct. As described previously, they can be changed.

Only one BroadcastReceiver is needed for programming work.

**Example of a BroadcastReceiver:**

```java
public class BarcodeReceiver extends BroadcastReceiver {

  public static final String ACTION_BARCODE = 
    "de.acdgruppe.hasciservice.data";  // In HasciDataService configured
                                       // Intent Action

  private static final String TAG = "BarcodeReceiver";

  @Override
  public void onReceive(Context context, Intent intent) {
    String action = intent.getAction();
    Bundle b = intent.getExtras();

    Log.d(TAG, "onReceive");
    if (action.equals(ACTION_BARCODE)) {
      Log.d(TAG, String.format(Locale.US,
                "Action equals ACTION_BARCODE: %s", ACTION_BARCODE));

      try {
        displayScanResult(intent);
      } catch (Exception e) {
          e.printStackTrace();
      }
    }
  }

  private void displayScanResult(Intent initiatingIntent)
  {
    String decodedData = initiatingIntent.getStringExtra(
           "de.acdgruppe.hasciservice.barcode.data");

    Log.i(TAG, "barcodeData: "+decodedData);

    String barcodeType = initiatingIntent.getStringExtra(
           "de.acdgruppe.hasciservice.barcode.type");

    Log.i(TAG, "barcodeType: "+barcodeType);

    MainActivity._txtBarcodeData.setText(decodedData);
    MainActivity._txtBarcodeType.setText(barcodeType);
  }
}
```

**Example of using the BroadcastReceiver:**

```java
public class MainActivity extends AppCompatActivity {
  
  public static TextView _txtBarcodeData;
  public static TextView _txtBarcodeType;

  BarcodeReceiver _barcodeReceiver;
  IntentFilter _barcodeIntentFilter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    _txtBarcodeData = (TextView) findViewById(R.id.txtBarcodeData);
    _txtBarcodeType = (TextView) findViewById(R.id.txtBarcodeType);

    _barcodeReceiver = new BarcodeReceiver();

     _barcodeIntentFilter = new IntentFilter(
         BarcodeReceiver.ACTION_BARCODE);  // In HasciDataService configured
                                           // Intent Action

    _barcodeIntentFilter.addCategory(
        Intent.CATEGORY_DEFAULT);         // In HasciDataService configured
                                          // Intent Categoty

    registerReceiver(_barcodeReceiver, _barcodeIntentFilter);
  }
}
```

### Using of a Messenger

The ACD HasciDataService can be controlled by a bind mechanism. To achieve this, the class ServiceConnection is used.

#### Bind on a service

To bind on a service (e.g. the ACD HasciDataService) the following example can be used:

```java
try {
  Intent acdIntent = new Intent();
  ComponentName acdName = new ComponentName(
    "de.acdgruppe.hasciservice",
    "de.acdgruppe.hasciservice.HasciService");
  acdIntent.setComponent(acdName);
  if (this.getApplicationContext().bindService(
      acdIntent, moduleConnection, Context.BIND_AUTO_CREATE)) {
    Log.i(TAG, "HasciService connect");
    isConnected = true;
  } else {
    Log.w(TAG, "false, can't connect to HasciService");
  }
} catch (Exception e) {
    Log.e(TAG, "false, can't connect to HasciService | " + e.getMessage());
}
```

The constructor used is ComponentName(String pkg, String cls), which takes the name of the target package pkg and the class cls of the service on which the binding should be performed.


#### The ServiceConnection

The ServiceConnection class must be implemented as follows:

```java
ServiceConnection moduleConnection = new ServiceConnection() {
  @Override
  public void onServiceConnected(ComponentName name, IBinder service) {

    try {
      Log.v(TAG, "send initial WHAT_HELLO");
      moduleMessenger = new Messenger(service);
      sendServiceMessage(null, WHAT_HELLO, 0, 0);
    } catch (Exception e) {
      Log.e(TAG, "false, Error on init M2ModuleService");
    }
  }

  @Override
  public void onServiceDisconnected(ComponentName name) {
  }
};
```

The methods onServiceConnected() and onServiceDisconnected() have to be overridden.
 After the application has finished binding on the targeted service, onServiceConnected() is called.

#### Initialize Communication

To initialize the communication between application and service a new Messenger object moduleMessenger is instantiated and the first message is sent. Therefore the following wrapper function is used:

```java
private boolean sendServiceMessage(Bundle bundle, int what, int arg1, int arg2){
  boolean result = false;
  try {
    Message moduleMessage = Message.obtain(null, what, arg1, arg2);

    if (null != bundle) {
      System.out.println("bundle data: " + bundle.getString("configCommand"));
      moduleMessage.setData(bundle);
    }

    moduleMessage.replyTo = moduleReceiver;
    moduleMessenger.send(moduleMessage);
    moduleMessage.getCallback();
    result = true;
  } catch (Exception e) {
    Log.e(TAG, "false, Error on HasciService communication");
  }
  return result;
}
```

The message consists of an optional bundle, the integer what which defines the type of message which is sent and two further arguments arg1 and arg2.

The first message to initialize communication contains only the what-message WHAT\_HELLO (arg1 and arg2 are zero). With the WHAT\_HELLO message the application draws attention to itself. This is answered by the ACD HasciDataService with a WHAT\_OK message.

The replyTo field takes a Messenger object which defines the answer channel from Service to Application.

#### Receive messages

To receive answer messages from ACD HasciDataService a new Messenger object has to be instantiated and registered in the replyTo field when binding on the service. The following listing can be used as reference:

```java
private Messenger moduleReceiver = new Messenger(new Handler(new Callback() {
  @Override
  public boolean handleMessage(Message msg) {

    Log.v(TAG, msg.toString());
    Bundle bundle = msg.getData();

    switch (msg.what) {
    case WHAT_OK:
      Log.i(TAG, "response from service was WHAT_OK");

    // Handle the WHAT messages !!!
    }
    return true;
  }
}));
```


#### Answer messages

To extract scan data the following example can be used. "msg" represents the message object from the handleMessage() function.

```java
Bundle bundle = msg.getData();
...
// extract data depending on the content of msg.what
...
case WHAT_HASCI_SCANDATA:

  String scanData = bundle.getString("scanData");
  Log.v(TAG, "scanData: "+scanData);

  String barcodeType =  bundle.getString("barcodeType");
  if (null != barcodeType)
    Log.v(TAG, "barcodeType: "+barcodeType);
  break;
```

#### Send messages

##### Scan and Connect

To toggle a scan and connect procedure with a HasciSE in range the following function call can be used:

```java
sendServiceMessage(null, WHAT_HASCI_CONNECT,0,0);
```

The what message WHAT\_HASCI\_CONNECT is mandatory.


##### Configurate HasciSE

To configurate the HasciSE connected to the android device the optional bundle can be used as follows:

```java
Bundle bundle = new Bundle();
String configCommand = ...+"\n";
bundle.putString("configCommand", configCommand);
sendServiceMessage(bundle, WHAT_HASCI_CONFIG, 0,0);
```

The key "configCommand" inside the bundle and the message WHAT\_HASCI\_CONFIG are mandatory.

To extract configuration data the following example can be used. "msg" represents the Message object from the handleMessage() function.

```java
Bundle bundle = msg.getData();
...
// extract data depending on the content of msg.what
...
case WHAT_HASCI_CONFIG:
  Log.i(TAG, "received WHAT_HASCI_CONFIG");

  String configData = bundle.getString("configData");
  Log.v(TAG, "configData: "+configData);
  break;
  ```


### WHAT Messages

Data is exchanged between an app and the HasciDataService via messages with bundles.

#### Message Structure

| **Parameter** | **Type** | **Explanations** |
| --- | --- | --- |
| Data | Bundle | A bundle of various data items. Can also be zero. |
| What | Int | The message ID. |
| Arg1 | Int | Argument 1. |
| Arg2 | Int | Argument 2. |

#### The message IDs

| **Message-ID** | **Value** | **Explanations** |
| --- | --- | --- |
| WHAT\_ERROR | 0 | Not in use yet. Available for expansions. |
| WHAT\_OK | 1 | Positive responses to WHAT messages of the calling app. |
| WHAT\_HELLO | 2 | The app uses this message to query the ready status of the HasciDataService. |
| WHAT\_HASCI\_CONNECT | 9 | Sent by the app: <br/> Command to set up the connection to a HasciSE. |
| WHAT\_HASCI\_CONNECT | 9 | Sent by the HasciDataService: <br/> Message reporting the connection status. |
| WHAT\_HASCI\_CONFIG | 10 | Configuration commands to and responses of HasciSE. |
| WHAT\_HASCI\_SCANDATA | 11 | Scan data. Barcode and barcode type. |
| WHAT\_HASCI\_ATTRIB | 12 | Queries and responses to attributes of HasciSE. |
| WHAT\_HASCI\_UPDATE\_CMD | 20 | Command for firmware update of the connected HasciSE. |
| WHAT\_HASCI\_UPDATE\_RSP | 21 | Responses for firmware update of the connected HasciSE. |

#### Message WHAT\_HELLO

From version 1.53 of the app, there is an additional option available for the WHAT\_HELLO message.

The variable part of the pairing barcode can be received via an optional bundle and saved for further use:

| **Key** | **Value** | **Explanations** |
| --- | --- | --- |
| variableBarcodePart | String | Variable part of the pairing barcode |

The variable part has 12 places, consisting of the hexadecimal symbols 0 to 9 and A to F.

It can be generated from a Mac address. Or from any calculated value.

#### Response messages to the app

| **What** | **arg1** | **arg2** | **Explanations** |
| --- | --- | --- | --- |
| WHAT\_OK | WHAT\_HELLO | 1 | The HasciDataService is ready. |
| WHAT\_OK | WHAT\_HASCI\_CONNECT | 1 | Scanning has started. |
| WHAT\_OK | WHAT\_HASCI\_CONNECT | 2 | Scanning has not started. A device is still connected. |
| WHAT\_OK | WHAT\_HASCI\_CONNECT | 3 | Ready for communication. |
| WHAT\_OK | WHAT\_HASCI\_CONFIG | 1 | Configuration command is being executed. |
| WHAT\_OK | WHAT\_HASCI\_CONFIG | 2 | The connection to the HasciSE is still being set up. |
| WHAT\_OK | WHAT\_HASCI\_CONFIG | 3 | The connection to the HasciSE has been set up. |
| | | |
| WHAT\_HASCI\_CONNECT | 0 | 1 | A HasciSE has been connected. |
| WHAT\_HASCI\_CONNECT | 0 | 2 | Scanning has stopped. |
| WHAT\_HASCI\_CONNECT | 0 | 3 | The connection to the HasciSE has been terminated. |
| | | |
| WHAT\_HASCI\_CONFIG | 0 | 0 | Configuration data has been received and is present in the bundle. |
| WHAT\_HASCI\_CONFIG | 0 | 1 | Response with enabled status of the queried barcode type. |
| | | |
| WHAT\_HASCI\_SCANDATA | 0 | 1 | Scan data has been received and is present in the bundle. |
| | | |
| WHAT\_HASCI\_UPDATE\_CMD | 0 | 0 | The firmware update is starting. |
| WHAT\_HASCI\_UPDATE\_CMD | 1 | ErrNo | The firmware update could not be started. |
| WHAT\_HASCI\_UPDATE\_RSP | 0 | 0-100 | Progress level of the firmware update as a percent. |
| WHAT\_HASCI\_UPDATE\_RSP | 1 | ErrNo | Error during the firmware update (dedicated error list). |
| | | |
| WHAT\_HASCI\_ATTRIB | 0 | 1 | The attribute query is being executed. |
| WHAT\_HASCI\_ATTRIB | 0 | 2 | The connection to the HasciSE is still being set up. |
| WHAT\_HASCI\_ATTRIB | 0 | 3 | The values of the queried attributes are returned in the bundle. |
| WHAT\_HASCI\_ATTRIB | 1 | ErrNo | Error during the attribute query (dedicated error list). |

## Definition of the Message Bundle


### Receiving Scan Data in SSI Mode

The scan data is received via message WHAT\_HASCI\_SCANDATA.

The bundle in this message contains these data items:

| **Key** | **Value** | **Explanations** |
| --- | --- | --- |
| scanData | String | Scanned barcode as a string. |
| barcodeType | String | Type of the scanned barcode as a string. <br/> The itemization is significantly more detailed than in raw mode. |


#### Barcode Types as Text

This is a list of barcode types in text format:

```txt
"UPC-A"	
"UPC-A with 2 Supps."	
"UPC-A with 5 Supps."
"UPC-E"	"UPC-E with 2 Supps."	
"UPC-E with 5 Supps."
"UPC-E1"	
"UPC-E1 with 2 Supps."	
"UPC-E1 with 5 Supps."
"EAN-8"	
"EAN-8 with 2 Supps."	
"EAN-8 with 5 Supps."
"EAN13"	
"EAN13 with 2 Supps."	
"EAN13 with 5 Supps."
"Bookland"

"Code 128"	
"GS1 128"	
"Code 49"
"ISBT 128"	
"ISBT 128 Concat."	
"Code 16K"
"Code 39"	
"Code 39 Full ASCII"	
"Trioptic Code 39"
"Code 32"	
"Code 11"	
"Code 93"
"Discrete 2 of 5"	
"Interleaved 2 of 5"	
"IATA 2 of 5"
"Matrix 2 of 5"	
"Chinese 2 of 5"	
"Korean 3 of 5"
"Codabar"	"MSI"
"GS1 DataBar"	
"GS1 DataBar Limited"	
"GS1 DataBar Expanded"
"GS1 DataBar Expanded Coupon"

"QR Code"	
"Micro QR Code"	
"Macro QR Code"
"DotCode"	
"Aztec Code"	
"Aztec Rune Code"
"PDF 417"	
"Macro PDF 417"	
"Micro PDF"
"Micro PDF CCA"	
"Macro Micro PDF"	
"Maxicode"
"Data Matrix"	
"GS1 Data Matrix"
"Han Xin"	
"Grid Matrix"

"Postnet"	
"US Planet"	
"Japan Postal"
"Australia Postal"	
"Netherlands KIX Code"	
"Postbar CA"
"UK Postal"	
"4State US"	
"4State US4"

"Scanlet Webcode"	
"Cue CAT Code"	
"French Lottery"
"UPCD"	
"NW 7"	
"Coupon Code"
"Composite TLC-39"
"Composite CC-A GS1-128"	
"Composite CC-A EAN-13"
"Composite CC-A EAN-8"	
"Composite CC-A GS1-DB Expanded"
"Composite CC-A GS1-DB Limited"	
"Composite CC-A GS1-DataBar 14"
"Composite CC-A UPC-A"	
"Composite CC-A UPC-E"
"Composite CC-C GS1-128"	
"Composite CC-B GS1-128"
"Composite CC-B EAN-13"	
"Composite CC-B EAN-8"
"Composite CC-B GS1-DB Expanded"	
"Composite CC-B GS1-DB Limited"
"Composite CC-B GS1-DataBar 14"	
"Composite CC-B UPC-A"
"Composite CC-B UPC-E"
```


### Receiving Scan Data in Raw Mode

The scan data is received via message WHAT\_HASCI\_SCANDATA.

The bundle in this message contains these data items:

| **Key** | **Value** | **Explanations** |
| --- | --- | --- |
| scanData | String | Scanned barcode as a string. |
| barcodeType | String | Type of the scanned barcode as a string. <br/> Depending on the configuration in the HasciDataService and in the HasciSE (which can be changed by scanning a barcode with the HasciSE) there are four possibilities: <br/> No barcode ID: The value returned is always "UNKNOWN". <br/> AIM barcode ID: The value returned is always a three-place string in line with the manufacturer-independent AIM definition of barcode IDs. <br/> Symbol barcode ID: The value returned is always a one or three-place string in line with the proprietary Zebra definition of barcode IDs. <br/> Barcode ID as text: A description of the barcode type is returned in text format. |


#### Barcode IDs as Text

This is a list of barcode IDs in text format:

```txt
"EAN/UPC"
"Bookland" 
"Coupon Code"
"Code 39" 
"Trioptic Code 39" 
"Code 93"
"Code 128" 
"GS1 128" 
"GS1 DataBar Family"
"Codabar" 
"Interleaved 2 of 5" 
"Discrete 2 of 5"
"Matrix 2 of 5" 
"Chinese 2 of 5" 
"Korean 3 of 5"
"Code 11" 
"MSI" 
"UCC Composite, TLC 39"
"PDF417, ISSN EAN" "Aztec Code" "Data Matrix"
"GS1 Data Matrix" "QR Code" "GS1 QR"
"DotCode" "Maxicode" "Grid Matrix"
"Han Xin" "Postnet" "US Planet"
"Japan Postal" "UK Postal" "Netherlands KIX Code"
"Australia Post" "UPU FICS Postal" "Mailmark"

"USPS 4CBL"
```


### Structure of Commands and Responses


#### Sending Commands

Commands to the HasciSE are sent via message WHAT\_HASCI\_CONFIG.

The bundle in this message has the following structure:

| **Key** | **Value** | **Explanations** |
| --- | --- | --- |
| configCommand | String | Command as string. |
| _Optional further_ | _Optional further_ | Depending on the command, additional keys and values are included in the bundle. |


#### Receiving responses

The responses from the HasciSE are received via message WHAT\_HASCI\_CONFIG.

The bundle contains various data items depending on the type of response.


### Switch Commands


#### TRIGGER Command

| **Key** | **Value** | **Explanations** | **Response** |
| --- | --- | --- | --- |
| Command to trigger the scanner | None |
| configCommand | String | "TRIGGER" |


#### SHUTDOWN Command

| **Key** | **Value** | **Explanations** | **Response** |
| --- | --- | --- | --- |
| Command to turn off the HasciSE | None |
| configCommand | String | "SHUTDOWN" |


#### VIBRATE Command

| **Key** | **Value** | **Explanations** | **Response** |
| --- | --- | --- | --- |
| Command to trigger the vibration motor of the HasciSE | None |
| configCommand | String | "VIBRATE" |
| duration | Int | Duration in ms |


#### BEEP Command

| **Key** | **Value** | **Explanations** | **Response** |
| --- | --- | --- | --- |
| Command to trigger the HasciSE beeper | None |
| configCommand | String | "BEEP" |
| frequency | Int | Beep frequency in Hz |
| duration | Int | Duration in ms |


#### AUTOTRIGGER Command

| **Key** | **Value** | **Explanations** | **Response** |
| --- | --- | --- | --- |
| Activation command for auto trigger | None |
| configCommand | String | "AUTOTRIGGER" |
| permanent | Boolean | Permanent storage or not |
| enable | Boolean | Activation/deactivation of triggering via the proximity sensor |


#### SCANDELAY Command

| **Key** | **Value** | **Explanations** | **Response** |
| --- | --- | --- | --- |
| Scan delay for auto trigger | None |
| configCommand | String | "SCANDELAY" |
| permanent | Boolean | Permanent storage or not |
| duration | Int | Scan lock for the proximity sensor in ms <br/> 0 up to 12600 ms |


### Querying Basic Information

#### Query of Basic Information

| **Key** | **Value** | **Explanations** | **Response** |
| --- | --- | --- | --- |
| Query of basic information | Basic information response |
| configCommand | String | "INFO" |


#### Basic Information Response

| **Key** | **Value** | **Explanations** |
| --- | --- | --- |
| configData | String | All information as self-parsing string. <br/> Hasci;2;[FW];[BATFLAG];[VOLTAGE];[SERIALNR];[MAC];[HWVER] <br/> <br/> Hasci: Device family <br/> <br/> 2: Device variant 2: Second Edition <br/> <br/> FW: Firmware version, negative in bootloader mode <br/> <br/> BATFLAG: Battery-Flag, 0-BAT\_Empty; 1-BAT\_LOW; 2-BAT\_HIGH <br/> <br/> VOLTAGE: <br/> Battery voltage in mV <br/> SERIALNR: <br/> 12-place serial number <br/> <br/> MAC: <br/> MAC address of the HasciSE <br/> <br/> HARDWAREVER: <br/> Decimal hardware version of the HasciSE <br/> <br/> Example: Hasci;2;02.18.05;2;4160;196500001234;78:C4:0E:A0:6B:DE;4 |
| response | String | "INFO" |
| Model | String | Device family: Hasci |
| Variant | String | Device variant: 2 = Second Edition = HasciSE |
| Firmware | String | Firmware version |
| BatteryFlag | Int | Battery-Flag: 0 BAT\_Empty 1 BAT\_LOW 2 BAT\_HIGH |
| Voltage | Int | Battery voltage in mV |
| To be able to query **all** values, firmware version **2.18.04** or higher must be used. |
| SerialNr | String | Serial number |
| MACaddress | String | MAC address of the HasciSE |
| HardwareVer | Int | Hardware version |

### Barcode Activation and Query

#### Barcode Activation

| **Key** | **Value** | **Explanations** | **Response** |
| --- | --- | --- | --- |
| Command to activate / deactivate barcode types | None |
| configCommand | String | "SYMENABLE" |
| barcodeType | String | Name of the barcode type |
| permanent | Boolean | Permanent storage or not |
| enable | Boolean | Enable / disable the barcode type |

#### Query Barcode Activation Status

| **Key** | **Value** | **Explanations** | **Response** |
| --- | --- | --- | --- |
| Command to query the activation status of the barcode type | Barcode activation status response |
| configCommand | String | "SYMSTATUS" |
| barcodeType | String | Name of the barcode type |

#### Barcode Activation Status Response

| **Key** | **Value** | **Explanations** |
| --- | --- | --- |
| response | String | "SYMSTATUS" |
| barcodeType | String | Name of the barcode type |
| enabled | Boolean | Barcode type enabled / disabled |

#### Names for the Barcode Activation and Query

```txt
"UPC-A" 
"UPC-E" 
"UPC-E1"
"EAN-8" 
"EAN13" 
"Bookland"
"Code 128" 
"Code 39" 
"Code 93"
"Code 11" 
"Interleaved 2 of 5" 
"Codabar"
"MSI" 
"Matrix 2 of 5" 
"Korean 3 of 5"
"Postnet" 
"Micro PDF"
"Data Matrix"
"GS1 Data Matrix" 
"QR Code" 
"GS1 QR"
"Micro QR Code"
"Aztec Code" 
"DotCode"
"ISSN EAN"
"ISBT 128" 
"GS1 128"
"Discrete 2 of 5" 
"Chinese 2 of 5"
"Trioptic Code 39" 
"Code 39 Full ASCII"
"GS1 DataBar" 
"GS1 DataBar Limited" 
"GS1 DataBar Expanded"
"PDF 417" 
"Grid Matrix" 
"Han Xin"
"Maxicode" 
"USPS 4CBl" 
"Mailmark"
"Japan Postal" 
"UK Postal" 
"Netherlands KIX Code"
"Australia Post" 
"UPU FICS Postal" 
"US Planet"
 ```

### Querying Device Attributes

Parts in the HasciSE firmware are used for this purpose that were implemented as part of the general HasciSE firmware update. This is because the installed firmware version must be queried during the process.

In firmware version **2.17.04** or higher of the HasciSE, this query can be used to contain additional information beyond the INFO query.

From firmware version **2.18.04** , the additional information can be obtained through the INFO query.

Then the attribute query is no longer needed.

#### Query of the Devices Attributes

The **command** to query the devices attributes of the HasciSE is sent via message WHAT\_HASCI\_ATTRIB. The bundle in this message has the following structure:

| **Key** | **Value** | **Explanations** | **Response** |
| --- | --- | --- | --- |
| Query for attributes of the connected HasciSE | Response with the attributes |
| command | String | "ATTRIB" |

#### Response with the Attributes

The command is answered with the same message.

Three different cases are possible:

- If the value in argument 1 is 0 and the value in argument 2 is 1 or 2, the bundle is null.
- If the value in argument 1 is 0 and the value in argument 2 is 3, the bundle contains all values.
- If the value in argument 1 is 1, argument 2 contains an error number. The bundle contains entirely or partially invalid values.

| **Key** | **Value** | **Explanations** |
| --- | --- | --- |
| response | String | "ATTRIB" |
| Firmware | String | Firmware version |
| Voltage | Int | Battery voltage in mV |
| SerialNr | String | Serial number |
| MACaddress | String | MAC address of the HasciSE |
| HardwareVer | String | Hardware version |

The information included may have an effect on a planned firmware update:

- A firmware version that is already installed does not need to be installed.
- If the voltage is too low, do not attempt a firmware update.


## Definition of Message Bundles for the Firmware Update

### Starting a Firmware Update

The command for the firmware update of the HasciSE is sent via message WHAT\_HASCI\_UPDATE\_CMD.

The bundle in this message has the following structure:

| **Key** | **Value** | **Explanations** | **Response** |
| --- | --- | --- | --- |
| Command to start a firmware update | Response to starting the firmware update |
| command | String | "FWUPDATE" |
| filename | String | Absolute path to the binary for the firmware update. Example: <br/> /storage/emulated/0/ACD/Hasci **\_FW\_V\_** 02\_18\_05 **.DL.bin** |


### Response to Starting the Firmware Update

The command is answered with the same message. The bundle in this message has the following structure:

| **Key** | **Value** | **Explanations** |
| --- | --- | --- |
| response | String | "FWUPDATE" |
| started | Boolean | The firmware update has been started or not. |
| error | Int | Error number if started is false. |


### Possible Errors When Starting the Firmware Update

| **Error** | **Explanations** |
| --- | --- |
| 0 | No error. |
| 1 | No HasciSE is connected. |
| 2 | No update binary is indicated (null or empty string). |
| 3 | The update binary does not exist or its existence cannot be checked<br/>(lack of permissions?). |
| 4 | The name of the update binary has the wrong format (does not end with **.DL.bin** and does not contain **\_FW\_V\_** ). |
| 5 | The update binary cannot be read (lack of permissions?). |
| 6 | Voltage limit of the HasciSE for a firmware update not reached. The limit is 3.7 V. |


### Messages During the Firmware Update

During the firmware update, status messages are exchanged with message WHAT\_HASCI\_UPDATE\_RSP. The bundle in this message has the following structure:

| **Key** | **Value** | **Explanations** |
| --- | --- | --- |
| response | String | "FWUPDATE\_RESPONSE" |
| status | Int | 0: The firmware update is running or has ended successfully. 1: The firmware update has been interrupted due to an error. |
| progress | Int | Progress of the firmware update in 10% increments. |
| ready | Boolean | true: The firmware update has been successfully completed. |
| error | Int | Error number if status is 1. The error numbers are defined during development. |
| info | String | Information text of the current message. |


### Possible Errors During the Firmware Update

A list of possible error during the firmware update follows:  

| **Error number** | **Identifier** | **Explanation** |
| --- | --- | --- |
| 0 | Update\_Success | No error. |
| 1 | Update\_Pending | |
| 2 | Update\_Error | File access error. File does not exist or no read authorization. Not used in this app. The check is made here before the updateconnection is set up. |
| 3 | Update\_InProgress | |
| 4 | Update\_NotConnected | The update connection has not been set up. |
| 5 | Update\_Busy | An update is still running. |
| 6 | Update\_ErrorDatasize | |
| 7 | Update\_AlreadyConnected | A firmware update was interrupted, but the update connection is still intact. Turn HasciSE off and back on. Restart update. |
| 8 | Update\_UserDisconnect | Not used in this app. |
| 9 | Update\_UserCancel | Not used in this app. |
| 10 | Update\_AlreadySet | |
| 11 | Update\_ConnectionLost | Update connection interrupted.<br/> - Was the HasciSE moved out of range during the update? |
| 12 | Update\_Timeout | Transmit timeout exceeded.<br/> - Was the HasciSE moved out of range during the update? |
| 13 | Update\_ErrorChecksum | |
| 14 | Update\_UpdateNotNecessary | Not used in this app. |
| 15 | Update\_LowBattery | Not used in this app. The check is made here before the updateconnection is set up. |

## Notices

### Variants of the HasciDataService

From version 1.43 the app exists in multiple variants:

| **App name** | **Variant** | **System app** | **Explanations** |
| --- | --- | --- | --- |
| **HasciDataService** | deviceAcd | Yes | For ACD devices |
| deviceOther | No | For devices of other manufacturers |


### Authorizations and Difference between System App and Normal App

1. For BLE communication, the app variants generally require activated BT-compatible short-range radio in addition to authorization for the location service.
2. From Android 10, the apps also require the special app authorization for the location service.
 For Android 10, when foreground authorization of the location service is granted, background authorization is granted at the same time.
3. From Android 11, after foreground authorization is granted, background authorization is also granted.
4. For the possible HasciSE firmware update from version 1.43 or higher, authorization is also requested for photos, media and files.
5. From Android 11, in addition to point 4, access to all files must also be granted.

The authorizations for system apps are given automatically after the installation and cannot be changed.

The authorizations must be granted for normal apps:

- Use the special app authorization for the location service to select "**Allow all the time**"
 (foreground and background authorization for Android 10).
- If this is not displayed, select "**While using the app**"
 (foreground authorization for Android 11 and 12).
- Then for the following query select "**Allow all the time**"
 (background authorization for Android 11 and 12).
- With authorization "Access photos and media", select "**Allow**".
- With authorization "All files access", the switch must be activated.


### Recommendation for Installation

Due to the different app variants, it may happen that an incorrect APK is installed on devices. If the correct APLK is then installed by means of ADB, the error message **INSTALL\_FAILED\_UPDATE\_INCOMPATIBLE** may appear.

The following procedure is therefore recommended in general for the installation:

1. Uninstall any existing app version(s).
2. Make certain in the settings that the BT-compatible short-range radio and general location service are activated.
3. Install the correct app.
4. With the normal app, activate the app authorization for the location service.
5. With the normal app, activate the app authorization for photos, media and files.
6. Start the app.
7. Boot the device.

Up to Android 9, the app is started automatically while booting.